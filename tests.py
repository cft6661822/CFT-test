from fastapi.testclient import TestClient
from main import app
from DB import EmployeeDB, SalaryDB
from jose import jwt
import json

client = TestClient(app)

def test_login_for_access_token():
    response = client.post("/token", data={"username": "AAA", "password": "111"})
    assert response.status_code == 200
    assert "access_token" in response.json()

def test_read_salary_without_token():
    response = client.get("/salary")
    assert response.status_code == 401
    assert response.json() == {"detail": "Not authenticated"}

def test_read_salary_with_invalid_token():
    response = client.get("/salary", headers={"Authorization": "Bearer invalid_token"})
    assert response.status_code == 401
    assert response.json() == {"detail": "Неверный токен"}

def test_read_root():
    response = client.get("/")
    assert response.status_code == 200

