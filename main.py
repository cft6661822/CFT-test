from fastapi import Depends, FastAPI, HTTPException, Request
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from DB import EmployeeDB, SalaryDB, Session
from jose import jwt, JWTError
from authentications import create_access_token, get_db, authenticate_user, SECRET_KEY, ALGORITHM

app = FastAPI()

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")

templates = Jinja2Templates(directory="templates")

@app.post("/token")
async def login_for_access_token(form_data: OAuth2PasswordRequestForm = Depends(), db: Session = Depends(get_db)):
    employee = authenticate_user(db, form_data.username, form_data.password)
    if not employee:
        raise HTTPException(status_code=401, detail="Неверные учетные данные")
    
    access_token = create_access_token({"sub": employee.login})
    
    # Сохраняем токен в базе данных
    employee.token = access_token
    db.commit()
    
    return {"access_token": access_token, "token_type": "bearer"}

def get_current_user(token: str = Depends(oauth2_scheme), db: Session = Depends(get_db)):
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        username: str = payload.get("sub")
        if username is None:
            raise HTTPException(status_code=401, detail="Неверный токен")
    except JWTError:
        raise HTTPException(status_code=401, detail="Неверный токен")
    
    employee = db.query(EmployeeDB).filter(EmployeeDB.token == token).first()
    if not employee:
        raise HTTPException(status_code=401, detail="Доступ запрещен")

    
    return employee

@app.get("/salary")
async def read_salary(current_user: EmployeeDB = Depends(get_current_user), db: Session = Depends(get_db)):
    salary = db.query(SalaryDB).filter(SalaryDB.employee_id == current_user.id).first()
    if not salary:
        raise HTTPException(status_code=404, detail="Информация о зарплате не найдена")
    
    return {"current_salary": salary.current_salary, "next_raise_date": salary.next_raise_date}

@app.get("/", response_class=HTMLResponse)
async def read_root(request: Request):
    return templates.TemplateResponse("index.html", {"request": request})
