from fastapi import FastAPI
from fastapi.responses import JSONResponse
from fastapi.security import OAuth2PasswordBearer, OAuth2
from pydantic import BaseModel
from sqlalchemy import create_engine, Column, Integer, String, Float, DateTime 
from sqlalchemy.orm import declarative_base, sessionmaker
from datetime import datetime  

app = FastAPI()

engine = create_engine('sqlite:///salary_db.db')


Base = declarative_base()

class EmployeeDB(Base):
    __tablename__ = 'employees'
    id = Column(Integer, primary_key=True)
    login = Column(String)
    password = Column(String)
    token = Column(String)
    token_created_at = Column(DateTime, default=datetime.now)  

class SalaryDB(Base):
    __tablename__ = 'salaries'
    id = Column(Integer, primary_key=True)
    employee_id = Column(Integer)
    current_salary = Column(Float)
    next_raise_date = Column(String)

Base.metadata.create_all(engine)


Session = sessionmaker(bind=engine)
